import { Context } from "react";
import { createMachine } from "xstate";

type TrafficLightEvent =
  | { type: "NEXT" }
  | { type: "TURN_OFF" }
  | { type: "TURN_ON" };

type TraffiLightState =
  | { value: { ON: "green" }; context: undefined }
  | { value: { ON: "yellow" }; context: undefined }
  | { value: { ON: "red" }; context: undefined }
  | { value: "OFF"; context: undefined };

export const trafficLightMachine = createMachine<
  undefined,
  TrafficLightEvent,
  TraffiLightState
>(
  {
    id: "trafficLight",
    initial: "OFF",
    states: {
      ON: {
        invoke: {
          src: "check light stability",
          onDone: [
            {
              cond: "light is stable",
              target: "ON",
            },
            {
              cond: "light is not stable",
              target: "OFF",
            },
          ],
          onError: [
            {
              cond: "timeout",
              target: "OFF",
            },
            {
              target: "OFF",
            },
          ],
        },
        on: { TURN_OFF: "OFF" },
        initial: "red",
        states: {
          green: {
            on: { NEXT: "yellow" },
            after: {
              2000: "yellow",
            },
          },
          yellow: {
            on: { NEXT: "red" },
            after: {
              1500: "red",
            },
          },
          red: {
            on: { NEXT: "green" },
            after: {
              2000: "green",
            },
          },
        },
      },
      OFF: {
        on: {
          TURN_ON: "ON",
        },
      },
    },
  },
  {
    services: {
      "check light stability": async () => {
        await new Promise((resolve) => setTimeout(resolve, 1000));
        return "light is stable";
      },
    },
  }
);
